(function () {
  var orderKey = null;
  if(window.location.search.indexOf("repfully_source") > 0) {
    document.cookie = "repfully_source=" + getParameterByName("repfully_source");
    document.cookie = "repfully_campaign_id=" + getParameterByName("campaign_id");
    var paramsStr = "repfully_source=" + getCookie("repfully_source") + 
                    "&repfully_campaign_id=" + getCookie("repfully_campaign_id") +
                    "&company_token=" + _rep_company_token;

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", _rep_url + "/tracking/visit", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    xhttp.send(paramsStr);
  }

  if(window.location.href.indexOf("order-received") > 0) {
    orderKey = getParameterByName("key");
    var revenue = getElementByXpath("//table[contains(@class,'order_details')]/tfoot//td[1]/span/text()");
    var billingDetails =  getElementByXpath("//div[contains(@class,'addresses')]//address").innerHTML;
    var name = billingDetails.split("<br>").shift();
    var address = billingDetails.split("<br>").splice(1, 10).join(" ");
    var paramsStr = "repfully_source=" + getCookie("repfully_source") + 
                    "&repfully_campaign_id=" + getCookie("repfully_campaign_id") +
                    "&orderKey=" + orderKey +
                    "&company_token=" + _rep_company_token + 
                    "&products=" + getElementByXpath("//td[contains(@class,'product-name')]/a").text + " " +
                    "&quantity=" + getElementByXpath("//td[contains(@class,'product-name')]/strong/text()").textContent.replace("×", "").replace(/^\s+|\s+$/g,'') + 
                    "&revenue=" + revenue.textContent.replace("$", "").replace(".", "") +
                    "&name=" + name.replace(/^\s+|\s+$/g,'') + 
                    "&address=" + address +
                    "&email=" + getElementByXpath("//table[contains(@class,'customer_details')]//td[1]/text()").textContent + 
                    "&order_number=" + getElementByXpath("//li[contains(@class,'order')]/strong/text()").textContent
                    ;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", _rep_url + "/tracking/order", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    xhttp.send(paramsStr);
  }

})()

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
function getElementByXpath(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}