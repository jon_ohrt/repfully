include Warden::Test::Helpers
Warden.test_mode!


# Feature: Create company
#   As a user
#   I want to create a company
#   So I can visit protected areas of the site
feature 'Create company', :devise do
  after(:each) do
    Warden.test_reset!
  end

  # Scenario: Visitor cannot create a company  
  #   Given I am a visitor
  #   When visit create company page
  #   Then I see a root path
  scenario "Cannot create a company when logged out" do
    visit new_company_path
    expect(page).to have_content "You need to sign in or sign up before continuing."
  end

  # Scenario: User with rep role cannot create a company
  #    Given I am a rep
  #   When I visit create company page
  #   Then I see access denied

  scenario 'User with rep role cannot create a company' do
    user = FactoryGirl.create(:user, :rep)
    login_as(user, :scope => :user)
    visit new_company_path
    expect(page).to have_content "not authorized"
  end
  
  # Scenario: User must create a company  
  #   Given I am a new user
  #   When I sign in with valid credentials
  #   Then I see a new company page
  scenario 'User must create company' do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    visit root_path
    expect(page).to have_css '#new_company'
  end




end