desc "Reprocess Attachements"
task "reprocess" do
  Attachment.find_each do |ym|
    begin
      ym.file.cache_stored_file!
      ym.file.retrieve_from_cache!(ym.file.cache_name)
      ym.file.recreate_versions!
      ym.save!
    rescue => e
      puts  "ERROR: Attachment: #{ym.id} -> #{e.to_s}"
    end
  end
end
