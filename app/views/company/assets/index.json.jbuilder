json.array!(@assets) do |asset|
  json.extract! asset, :id, :asset_type, :title, :content
  json.url asset_url(asset, format: :json)
end
