json.array!(@company_campaigns) do |company_campaign|
  json.extract! company_campaign, :id, :name, :company_id
  json.url company_campaign_url(company_campaign, format: :json)
end
