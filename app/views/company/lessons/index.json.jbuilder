json.array!(@lessons) do |lessons|
  json.extract! lessons, :id, :title
  json.url lessons_url(lessons, format: :json)
end
