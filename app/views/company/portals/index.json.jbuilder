json.array!(@portals) do |portal|
  json.extract! portal, :id, :registration_content, :logo
  json.url portal_url(portal, format: :json)
end
