json.array!(@training_courses) do |training_course|
  json.extract! training_course, :id, :course_number
  json.url training_course_url(training_course, format: :json)
end
