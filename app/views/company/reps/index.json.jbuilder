json.array!(@reps) do |rep|
  json.extract! rep, :id, :first_name, :last_name, :email, :password, :password_confirmation
  json.url rep_url(rep, format: :json)
end
