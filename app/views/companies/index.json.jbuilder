json.array!(@companies) do |company|
  json.extract! company, :id, :name, :subdomain, :commission_value, :commission_type, :terms_value, :terms_type
  json.url company_url(company, format: :json)
end
