class VisitorsController < ApplicationController
  before_filter :require_company

  private
  def require_company
    if user_signed_in? && current_user.user? && current_user.companies.empty?
      redirect_to new_company_path unless request.path == new_company_path
    end
  end
end
