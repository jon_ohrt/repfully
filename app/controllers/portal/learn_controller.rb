class Portal::LearnController < Portal::AppController
  def show
    @training_courses = @selected_campaign.training_courses
    @selected_training_course = @training_courses.first
  end

  def complete_lesson
    #TODO remove this check
    unless LessonCompletion.where(lesson_id: params[:lesson_id], rep_id: current_rep.id).length > 0
      if @lesson = Lesson.find(params[:lesson_id])
        current_rep.completed_lessons << @lesson
      end
    end
    render :json => 200
  end

end