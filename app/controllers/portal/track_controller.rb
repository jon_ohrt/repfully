class Portal::TrackController < Portal::AppController
  def show
    @chart_data = current_rep.paid_balances_12_months
    @chart = LazyHighCharts::HighChart.new('graph') do |f|

      f.xAxis(categories: @chart_data.map{|d| d.keys.first})
      f.series(name: "Commission Amount", showInLegend: false, yAxis: 0, data: @chart_data.map {|c| c.values.first})
      f.chart({defaultSeriesType: "column", height: 150})
      f.yAxis(gridLineWidth: 0, minorGridLineWidth: 0, labels: {enabled: false}, title: {text: nil})
    end
    @chart_globals = LazyHighCharts::HighChartGlobals.new do |f|
      f.global(useUTC: false)
      f.lang(thousandsSep: ",")
      f.colors(["#ef7625"])
    end

    @events = current_rep.balances.collect {|b| b.event}
  end

end
