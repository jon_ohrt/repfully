class Portal::RegistrationsController < Devise::RegistrationsController
  layout 'portal_signed_out'
  include Portal::Concerns::SetCompany
  prepend_view_path 'app/views/portal'
  before_filter :sign_up_params, only: [:create, :update]

  def create
    build_resource(sign_up_params)
    @company.reps << resource
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  protected

  def sign_up_params
    params.require(:rep).permit(:first_name, :email, :password, :password_confirmation, :last_name, :website, :street_address, :street_address_2, :city, :country, :state, :zip, :phone, :alt_phone)
  end

end
