class Portal::AttachmentsController < Portal::AppController

  def show
    @file = Attachment.find(params[:id])
    send_file open(@file.file.url), type: @file.content_type,
                           disposition: 'attachment',
                           filename: File.basename(@file.file.url)

  end
end
