class Portal::ShareController < Portal::AppController
  def show
    @sendables = @selected_campaign.assets.sendables
    @sharables = @selected_campaign.assets.sharables
    @printables = @selected_campaign.assets.printables
  end

end