class Portal::PasswordsController < Devise::PasswordsController
  layout 'portal_signed_out'
  include Portal::Concerns::SetCompany
  prepend_view_path 'app/views/portal'
end
