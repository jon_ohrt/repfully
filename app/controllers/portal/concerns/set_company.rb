module Portal::Concerns::SetCompany
  extend ActiveSupport::Concern

  included do
    before_filter :set_company
  end

  def set_company
    @company = Company.find_by_subdomain(request.subdomain)
  end

end