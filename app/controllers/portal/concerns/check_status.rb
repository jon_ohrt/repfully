module Portal::Concerns::CheckStatus
  extend ActiveSupport::Concern

  included do
    before_filter :check_status
  end

  def check_status
    if current_rep.inactive?
      redirect_to "/inactive"
      return false
    end

    if current_rep.unconfirmed?
      redirect_to "u/nconfirmed"
      return false
    end
  end

end