class Portal::ConfirmationsController < ApplicationController
  layout 'portal_signed_out'
  include Portal::Concerns::SetCompany
  before_filter :find_rep

  def edit

  end

  def update
    respond_to do |format|
      if @rep.update(confirm_params)
        @rep.confirm!
        sign_in @rep
        format.html { redirect_to "", notice: 'Confiramtion was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @rep.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def find_rep
    @rep = Rep.find_by_token(params[:token])
    if @rep.nil?
      render :template => "portal/confirmations/not_found"
    end
    return false
  end

  # Never trust parameters from the scary internet, only allow the white list through.
    def confirm_params
      params.require(:rep).permit(:website, :street_address, :street_address_2, :city, :state, :zip, :country, :phone, :alt_phone)
    end
end

