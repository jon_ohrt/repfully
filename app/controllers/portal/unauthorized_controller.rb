class Portal::UnauthorizedController < ApplicationController
  include Portal::Concerns::SetCompany
  layout 'portal_signed_out'

  def inactive
    
  end

  def unconfirmed
    
  end

end