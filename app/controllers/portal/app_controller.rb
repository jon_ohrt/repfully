class Portal::AppController < ApplicationController
  before_action :authenticate_rep!
  include Portal::Concerns::SetCompany
  include Portal::Concerns::CheckStatus
  before_filter :set_campaigns
  before_filter :set_selected_campaign
  layout 'portal'

  private

  def set_campaigns
    @available_campaigns = current_rep.available_campaigns
  end

  def set_selected_campaign
    @selected_campaign = @available_campaigns.first
  end
end
