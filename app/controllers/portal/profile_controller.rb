class Portal::ProfileController < Portal::AppController
  before_filter :set_rep

  def edit

  end

  def update
   respond_to do |format|
      if @rep.update(rep_params)
        format.html { redirect_to portal_root_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @rep }
      else
        format.html { render :edit }
        format.json { render json: @rep.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_rep
    @rep = current_rep
  end
  

  # Never trust parameters from the scary internet, only allow the white list through.
  def rep_params
    params.require(:rep).permit(:first_name, :last_name, :website, :street_address, :street_address_2, :city, :state, :zip, :country, :phone, :alt_phone )
  end
end

