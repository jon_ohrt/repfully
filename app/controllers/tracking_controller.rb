class TrackingController < ApplicationController
  skip_before_filter  :verify_authenticity_token

  def order
    @event = Event.find_by_transaction_token(params[:orderKey])
    if @event.nil?
      event = Event.create(event_type: :transaction_made,
                   transaction_token: params[:orderKey],
                   rep: Rep.find_by_tracking_token(params[:repfully_source]),
                   campaign: Campaign.find_by_campaign_token(params[:repfully_campaign_id]),
                   company: Company.find_by_company_token(params[:company_token]),
                   ip_address: request.remote_ip, 
                   revenue_cents: params[:revenue],
                   name: params[:name],
                   email: params[:email],
                   address: params[:address],
                   products: params[:products],
                   quantity: params[:quantity],
                   order_number: params[:order_number])


    end
    
    head :ok  
  end

  def visit
    @event = Event.create(event_type: :visit,
                          rep: Rep.find_by_tracking_token(params[:repfully_source]),
                          campaign: Campaign.find_by_campaign_token(params[:repfully_campaign_id]),
                          company: Company.find_by_company_token(params[:company_token]) ,
                          ip_address: request.remote_ip)
    head :ok
  end

end