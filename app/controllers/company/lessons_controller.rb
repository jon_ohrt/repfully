class Company::LessonsController < Company::AppController
  before_action :set_course
  before_action :set_lessons, only: [:show, :edit, :update, :destroy]

  # GET /lessons
  # GET /lessons.json
  def index

    @lessons = @training_course.lessons.all
  end

  # GET /lessons/1
  # GET /lessons/1.json
  def show
  end

  # GET /lessons/new
  def new
    @lesson = @training_course.lessons.build
  end

  # GET /lessons/1/edit
  def edit
  end

  # POST /lessons
  # POST /lessons.json
  def create
    @lesson = @training_course.lessons.build(lessons_params)

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to company_training_course_lessons_path(@training_course), notice: 'Lession was successfully created.' }
        format.json { render :show, status: :created, location: @lesson }
      else
        format.html { render :new }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lessons/1
  # PATCH/PUT /lessons/1.json
  def update
    respond_to do |format|
      if @lesson.update(lessons_params)
        format.html { redirect_to company_training_course_lessons_path(@training_course), notice: 'Lession was successfully updated.' }
        format.json { render :show, status: :ok, location: @lesson }
      else
        format.html { render :edit }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lessons/1
  # DELETE /lessons/1.json
  def destroy
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to company_training_courses_url(@training_course), notice: 'Lession was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_course
      @training_course = @company.training_courses.find(params[:training_course_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_lessons
      @lesson = @training_course.lessons.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lessons_params
      params.require(:lesson).permit(:title, :description, :lesson_parts_attributes => [:lesson_part_type, :text_section, :action_link_text, :action_link_file, :action_link_url, :link_type, :show_icon, :image, :id, :_destroy, :attachment_attributes => [:file, :id, :_destroy]])
    end
end
