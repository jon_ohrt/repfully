class Company::BalancesController < Company::AppController
  before_filter :set_rep
  before_filter :set_balance

  def show
  end

  def update
    respond_to do |format|
      if @balance.update(balance_params)
        format.html {
          redirect_to edit_company_rep_path(@balance.rep),
                      notice: 'Company campaign was successfully updated.'
        }
        format.json { render :show, status: :ok, location: @balance }
      else
        format.html { render :edit }
        format.json { render json: @balance.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_rep
    @rep = Rep.find(params[:rep_id])
  end

  def set_balance
    @balance = @rep.balances.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def balance_params
    params.require(:balance).permit(:paid)
  end
end
