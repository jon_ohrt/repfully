class Company::AppController < ApplicationController
  before_action :authenticate_user!
  before_filter :set_company
  
  layout 'company'

  def set_company
    @company = current_user.company
  end
end
