class Company::RepsController < Company::AppController
  before_action :set_rep, only: [:show, :edit, :update, :destroy]
  before_action :filter_password_params, only: [:update]
  before_action :filter_group_ids, only: [:update]
  # GET /reps
  # GET /reps.json
  def index
    @reps = @company.reps.all
  end

  # GET /reps/1
  # GET /reps/1.json
  def show
  end

  # GET /reps/new
  def new
    @rep = @company.reps.build
  end

  # GET /reps/1/edit
  def edit
  end

  # POST /reps
  # POST /reps.json
  def create
    @rep = @company.reps.build(rep_params.merge!(admin_created: true))

    respond_to do |format|
      if @rep.save
        format.html { redirect_to company_reps_path, notice: 'Rep was successfully created.' }
        format.json { render :show, status: :created, location: @rep }
      else
        format.html { render :new }
        format.json { render json: @rep.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reps/1
  # PATCH/PUT /reps/1.json
  def update
    respond_to do |format|
      if @rep.update(rep_params)
        format.html { redirect_to company_reps_path, notice: 'Rep was successfully updated.' }
        format.json { render :show, status: :ok, location: @rep }
      else
        format.html { render :edit }
        format.json { render json: @rep.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reps/1
  # DELETE /reps/1.json
  def destroy
    @rep.destroy
    respond_to do |format|
      format.html { redirect_to company_reps_url, notice: 'Rep was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rep
      @rep = @company.reps.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rep_params
      params.require(:rep).permit(:first_name, :last_name, :email, :password, :password_confirmation, :website, :street_address, :street_address_2, :city, :state, :zip, :country, :phone, :alt_phone, :status, :group_ids => [] )
    end

    def filter_password_params
      if params[:rep][:password].blank?
        params[:rep].delete(:password)
        params[:rep].delete(:password_confirmation)
      end
    end

    def filter_group_ids
      if params[:rep][:group_ids].nil?
        params[:rep][:group_ids] = []
      end
    end
end
