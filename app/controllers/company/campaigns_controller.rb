class Company::CampaignsController < Company::AppController
  before_action :set_company
  before_action :set_campaign, only: [:show, :edit, :update, :destroy]
  before_action :filter_group_ids, only: [:update]
  before_action :filter_training_course_ids, only: [:update]
  before_action :filter_product_group_ids, only: [:update]
  # GET /campaigns
  # GET /campaigns.json
  def index
    @campaigns = @company.campaigns
  end

  # GET /campaigns/1
  # GET /campaigns/1.json
  def show
  end

  # GET /campaigns/new
  def new
    @campaign = @company.campaigns.build
    @campaign.create_group_if_none_exists
  end

  # GET /campaigns/1/edit
  def edit
  end

  # POST /campaigns
  # POST /campaigns.json
  def create
    @campaign = @company.campaigns.build(campaign_params)
    respond_to do |format|
      if @campaign.save
        format.html { redirect_to company_campaigns_url, notice: 'Company campaign was successfully created.' }
        format.json { render :show, status: :created, location: @campaign }
      else
        format.html { render :new }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /campaigns/1
  # PATCH/PUT /campaigns/1.json
  def update
    respond_to do |format|
      if @campaign.update(campaign_params)
        format.html { redirect_to company_campaigns_url notice: 'Company campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaigns/1
  # DELETE /campaigns/1.json
  def destroy
    @campaign.destroy
    respond_to do |format|
      format.html { redirect_to company_campaigns_url,
                                notice: 'Company campaign was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_company
    @company = current_user.company
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_campaign
    @campaign = @company.campaigns.find(params[:id])
  end

  def filter_group_ids
    return if params[:campaign][:group_ids].nil?
    params[:campaign][:group_ids] = []
  end

  def filter_training_course_ids
    if params[:campaign][:training_course_ids].nil?
      params[:campaign][:training_course_ids] = []
    end
  end

  def filter_product_group_ids
    return if params[:campaign][:product_group_ids].nil?
    params[:campaign][:product_group_ids] = []
  end

  # Never trust parameters from the scary internet,
  # only allow the white list through.
  def campaign_params
    params.require(:campaign).permit(:name,
                                     :website,
                                     group_ids: [],
                                     product_group_ids: [],
                                     commission_rule_attributes:
                                        [:commission_value,
                                         :commission_type,
                                         :terms_value,
                                         :terms_type])
  end
end
