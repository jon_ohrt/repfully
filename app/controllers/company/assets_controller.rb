class Company::AssetsController < Company::AppController
  before_action :set_asset, only: [:show, :edit, :update, :destroy]
  before_action :filter_product_group_ids, only: [:update]
  # GET /assets
  # GET /assets.json
  def index
    @assets = @company.assets.all
  end

  # GET /assets/1
  # GET /assets/1.json
  def show
  end

  # GET /assets/new
  def new
    @asset = @company.assets.new
  end

  # GET /assets/1/edit
  def edit
  end

  # POST /assets
  # POST /assets.json
  def create
    @asset = @company.assets.build(asset_params)

    respond_to do |format|
      if @asset.save
        format.html { redirect_to company_assets_path, notice: 'Asset was successfully created.' }
        format.json { render :show, status: :created, location: @asset }
      else
        format.html { render :new }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assets/1
  # PATCH/PUT /assets/1.json
  def update
    respond_to do |format|
      if @asset.update(asset_params)
        format.html { redirect_to company_assets_path, notice: 'Asset was successfully updated.' }
        format.json { render :show, status: :ok, location: @asset }
      else
        format.html { render :edit }
        format.json { render json: @asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assets/1
  # DELETE /assets/1.json
  def destroy
    @asset.destroy
    respond_to do |format|
      format.html { redirect_to company_assets_url, notice: 'Asset was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asset
      @asset = @company.assets.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asset_params
      params.require(:asset).permit(:asset_type, :title, :content, :product_group_ids => [], :attachment_attributes => [:id, :file, :_destroy])
    end

    def filter_product_group_ids
      if params[:asset][:product_group_ids].nil?
        params[:asset][:product_group_ids]= []
      end
    end
end
