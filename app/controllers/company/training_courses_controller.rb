class Company::TrainingCoursesController < Company::AppController
  before_action :set_training_course, only: [:show, :edit, :update, :destroy]
  before_action :filter_product_group_ids, only: [:update]
  # GET /training_courses
  # GET /training_courses.json
  def index
    @training_courses = @company.training_courses.all
  end

  # GET /training_courses/1
  # GET /training_courses/1.json
  def show
  end

  # GET /training_courses/new
  def new
    @training_course = @company.training_courses.build
  end

  # GET /training_courses/1/edit
  def edit
  end

  # POST /training_courses
  # POST /training_courses.json
  def create
    @training_course = @company.training_courses.build(training_course_params)

    respond_to do |format|
      if @training_course.save
        format.html { redirect_to company_training_courses_path, notice: 'Training course was successfully created.' }
        format.json { render :show, status: :created, location: @training_course }
      else
        format.html { render :new }
        format.json { render json: @training_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /training_courses/1
  # PATCH/PUT /training_courses/1.json
  def update
    respond_to do |format|
      if @training_course.update(training_course_params)
        format.html { redirect_to company_training_courses_path, notice: 'Training course was successfully updated.' }
        format.json { render :show, status: :ok, location: @training_course }
      else
        format.html { render :edit }
        format.json { render json: @training_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /training_courses/1
  # DELETE /training_courses/1.json
  def destroy
    @training_course.destroy
    respond_to do |format|
      format.html { redirect_to company_training_courses_url, notice: 'Training course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training_course
      @training_course = @company.training_courses.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_course_params
      params.require(:training_course).permit(:course_number, :title, :description, :product_group_ids => [])
    end

    def filter_product_group_ids
      if params[:training_course][:product_group_ids].nil?
        params[:training_course][:product_group_ids]= []
      end
    end
end
