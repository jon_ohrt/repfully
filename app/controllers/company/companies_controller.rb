class Company::CompaniesController < Company::AppController
  before_filter :set_company
  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = current_user.build_company
    authorize @company
  end

  # GET /companies/1/edit
  def edit
  end

  def update
    respond_to do |format|
      if @company.update_attributes(company_params)
        format.html { redirect_to edit_company_company_path, notice: 'Company was successfully updated.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
  def set_company
    @company = current_user.company
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def company_params
    params.require(:company).permit(:name, :subdomain, :website)
  end
end
