module CompaniesHelper

  def nav_item(text, path, opts={})
    content_tag :li, {class: "#{'active' if params[:controller].include?(path)}"} do
      link_to text, path, opts
    end
  end
end
