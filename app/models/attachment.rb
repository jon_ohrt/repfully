class Attachment < ActiveRecord::Base
  belongs_to :attachable, polymorphic: true
  mount_uploader :file, FileUploader

  #TODO: Make sure attachments are updatable in lesson parts instead of creating new assets every time

end
