class TrainingCourse < ActiveRecord::Base
  belongs_to :company
  belongs_to :campaign
  has_many :lessons

  has_and_belongs_to_many :product_groups
end
