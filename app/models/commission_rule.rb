class CommissionRule < ActiveRecord::Base
  belongs_to :campaign

  enum commission_type: [:percent, :per_sale]
  enum terms_type: [:days, :weeks, :months]

  def self.build_default_rule
    CommissionRule.new(:commission_value => 10, 
                         :commission_type => :percent,
                         :terms_value => 10,
                         :terms_type => :days)
  end

  def calculate_commission(event)
    if percent?
      return event.revenue * (commission_value / 100)
    elsif per_sale?
      return event.quantity * commission_value
    end
  end
end
