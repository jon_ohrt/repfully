class Company < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :subdomain

  after_create :set_default_settings
  before_create :generate_token
  after_initialize :create_portal

  has_many :users
  has_many :campaigns
  has_many :groups
  has_many :reps
  has_many :training_courses
  has_many :asset_groups
  has_many :assets

  has_many :events
  has_one  :portal
  accepts_nested_attributes_for :portal

  has_many :product_groups
  
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders] 

  COMMISSION_TYPES = ["percent", "per sale"]
  TERMS_TYPES = ["days", "weeks", "months"]

  private

  def set_default_settings
    self.update_columns(:commission_value => 10, 
                        :commission_type => "percent",
                        :terms_value => 10,
                        :terms_type => "days")
  end

  def create_portal
    self.portal = Portal.new if self.portal.nil?
  end

  def generate_token
    self.company_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(company_token: random_token)
    end
  end

end
