class Balance < ActiveRecord::Base
  belongs_to :event
  belongs_to :rep

  monetize :amount_cents

  enum amount_type: [:positive, :negative]

  after_save:update_rep_totals

  scope :paid, lambda {
    where(paid: true)
  }

  scope :unpaid, lambda {
    where(paid: false)
  }

  scope :this_year, lambda {
    #TODO, reset this total at beginning of year
    where("CAST(created_at AS DATE) >= ?", Date.today.beginning_of_year)
  }

  scope :last_month, lambda {
    where("CAST(created_at AS DATE) >= ? and CAST(created_at AS DATE) <= ?", (Date.today - 1.months).beginning_of_month, (Date.today - 1.months).end_of_month )
  }

  scope :this_month, lambda {
    where("CAST(created_at AS DATE) >= ?", Date.today.beginning_of_month)
  }

  scope :between, lambda { |date_begining, date_end|
    where("CAST(created_at AS DATE) >= ? and CAST(created_at AS DATE) <= ?", date_begining, date_end)
  }
  def self.add(rep, event, amount, due_date)
    rep.balances.create(event: event, amount_cents: amount.cents, amount_type: :positive, due_date: due_date)
  end


  private

  def update_rep_totals
    rep.update_totals
  end

end
