class Rep < ActiveRecord::Base
  has_and_belongs_to_many :groups
  belongs_to :company
  has_many :lesson_completions
  has_many :completed_lessons,-> { uniq }, through: :lesson_completions, class_name: "Lesson", source: :lesson

  has_many :events

  has_many :balances

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :set_status
  after_create :assign_to_group
  before_create :generate_tokens

  has_many :commissions

  monetize :sales_ytd_cents
  monetize :commission_ytd_cents
  monetize :sales_since_active_cents
  monetize :commission_since_active_cents
  monetize :unpaid_commissions_cents

  enum status: [:unconfirmed, :active, :inactive, :suspended]

  validates_presence_of :street_address, unless: :admin_created
  validates_presence_of :city, unless: :admin_created
  validates_presence_of :state, unless: :admin_created
  validates_presence_of :zip, unless: :admin_created
  validates_presence_of :country, unless: :admin_created
  validates_presence_of :phone, unless: :admin_created

  attr_accessor :admin_created

  def full_name
    "#{first_name} #{last_name}"
  end

  def confirm!
    update_attributes(status: "inactive", confirmed_at: Time.now)
  end

  def available_campaigns
    Campaign.joins(:groups).where("groups.id in (?)", group_ids)
  end

  def update_totals
    sales_ytd_cents = events.this_year.sum(:revenue_cents)
    commission_ytd_cents = balances.this_year.sum(:amount_cents)
    sales_since_active_cents = events.sum(:revenue_cents)
    commission_since_active_cents = balances.sum(:amount_cents)
    unpaid_commissions_cents = balances.unpaid.sum(:amount_cents)
    next_due_date = balances.unpaid.minimum(:due_date)
    update_attributes(sales_ytd_cents: sales_ytd_cents,
                      commission_ytd_cents: commission_ytd_cents,
                      sales_since_active_cents: sales_since_active_cents,
                      commission_since_active_cents: commission_since_active_cents,
                      unpaid_commissions_cents: unpaid_commissions_cents,
                      next_commission_due: next_due_date)
  end

  def paid_balances_12_months
    (0..11).map do |n|
      {(Date.today - n.months).strftime("%B") => balances.paid.between((Date.today - n.months).beginning_of_month, (Date.today - n.months).end_of_month).sum(:amount_cents) / 100}
    end.reverse
  end

  private

  def set_status
    if admin_created
      self.status = :unconfirmed
    else
      self.status = :inactive
    end
  end

  def assign_to_group
    if company.groups.empty?
      group = company.groups.create_default_group
      groups << group
      company.groups << group
    else
      company.groups.where(default: true).first.reps << self
    end
  end

  def generate_tokens
    #TODO change token to confirm_token
    tokens = [:token, :tracking_token]
    tokens.each do |token|
      token_str = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless self.class.exists?(token => random_token)
      end
      self.send("#{token}=", token_str)
    end
  end
end
