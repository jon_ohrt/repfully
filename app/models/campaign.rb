class Campaign < ActiveRecord::Base
  belongs_to :company

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders] 


  has_and_belongs_to_many :groups
  has_and_belongs_to_many :product_groups
  has_many :training_courses, through: :product_groups
  has_one :commission_rule
  accepts_nested_attributes_for :commission_rule
  
  has_many :events
  
  before_create :generate_token
 

  after_initialize :create_commission_rule_if_none_exist

  has_and_belongs_to_many :product_groups
  has_many :assets, through: :product_groups
  
  
  def referral_link(rep)
    company.website + "?repfully_source=#{rep.tracking_token}&campaign_id=#{self.campaign_token}"  
  end


  def reps
    groups.collect do |group|
      group.reps
    end.flatten
  end

  def create_group_if_none_exists
    if company.groups.empty?
      group = Group.create_default_group(company)
      self.groups << group
    else
      self.groups << Group.where(default: true).first
    end
    if company.product_groups.empty?
      product_group = ProductGroup.create_default_group(company)
      self.product_groups << product_group
    else
      self.product_group << ProductGroup.where(default: true).first
    end
  end

  private

  def create_commission_rule_if_none_exist
    self.commission_rule = CommissionRule.build_default_rule if commission_rule.nil?
  end

  
    

  def generate_token
    self.campaign_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(campaign_token: random_token)
    end
  end
 

end
