class Commission < ActiveRecord::Base
  belongs_to :rep
  belongs_to :customer

  monetize :sale_amount_cents
  monetize :commission_amount_cents

  enum commission_status: [:unpaid, :approved, :denied, :paid]

  def self.add_from_event(event)
    commission = Commission.new
    commission.rep = event.rep
    company = commission.rep.company
    commission.sale_amount_cents = event.revenue_cents
    commission.commission_amount_cents = calculate_commission(event.revenue_cents, company)
    commission.customer = Customer.find_or_create(email: event.email)
    commission.customer.name = event.name
    commission.customer.address = event.address
    commission.commission_due_date = calculate_due_date(company)
    commission.sale_date = Date.today
    commission.commission_status = :unpaid
    commission.save!
  end

  def self.calculate_commission(price, company)
    if company.commission_type = "percent"
      return price / company.commission_value
    else
      return company.commission_value * 100
    end
  end

  def self.calculate_due_date(company)
    if company.terms_type == "days"
      return Date.today + company.term_value.days
    elsif company.terms_type == "weeks"
      return Date.today + company.term_value.weeks
    else
      return Date.today + company.term_value.months
    end
  end
end
