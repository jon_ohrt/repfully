class ProductGroup < ActiveRecord::Base
  belongs_to :company
  validates_presence_of :name
  has_and_belongs_to_many :assets
  has_and_belongs_to_many :training_courses

  after_save :set_as_default


  def self.create_default_group(company)
    company.product_groups.create(:name => "Default Group", :default=> true)
  end

  private

  def set_as_default 
    if default == true
      company.product_groups.where("id != ?", id).each do |product_group|
        product_group.update_columns(default: false)
      end
    end
  end
end
