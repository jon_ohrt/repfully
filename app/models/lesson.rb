class Lesson < ActiveRecord::Base
  belongs_to :training_courses
  has_many :lesson_parts
  belongs_to :portal, foreign_key: :training_overview_id 
  accepts_nested_attributes_for :lesson_parts
  has_many :lesson_completions

  def has_video_part?
    lesson_parts.video.length > 0
  end

  def completed(rep)
    LessonCompletion.where(rep_id: rep.id, lesson_id: self.id).length > 0
  end
end
