# Lesson part is a section of a lesson
class LessonPart < ActiveRecord::Base
  belongs_to :lesson
  has_one :attachment, as: :attachable
  accepts_nested_attributes_for :attachment
  enum lesson_part_type: [:text, :video, :resource]
  enum link_type: [:asset, :url]
  mount_uploader :image, LessonImageUploader
end
