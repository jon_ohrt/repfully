class Asset < ActiveRecord::Base
  belongs_to :company
  has_and_belongs_to_many :product_groups
  has_one :attachment, as: :attachable
  accepts_nested_attributes_for :attachment

  after_initialize :create_attachment
  after_initialize :create_product_group_if_none_exist
  enum asset_type: [:sharable, :sendable, :printable]

  scope :sharables, lambda {
    #TODO figure out why eI can find by :sendable
    where(asset_type: 0)
  } 

  scope :sendables, lambda {
    #TODO figure out why eI can find by :sendable
    where(asset_type: 1)
  } 
  scope :printables, lambda {
    #TODO figure out why eI can find by :sendable
    where(asset_type: 2)
  } 
  def create_attachment
    self.build_attachment if self.attachment.nil?
  end

  def create_product_group_if_none_exist
    if company.product_groups.empty?
      asset_product = company.product_groups.create(name: "Default group")
      product_group.assets << self
    end
  end
end
