class Event < ActiveRecord::Base
  belongs_to :rep
  belongs_to :company
  belongs_to :campaign
  has_one :balance
  #TODO: Rename :transaction_made
  enum event_type: [:transaction_made, :visit]

  monetize :revenue_cents
  after_create :calculate_commission

   scope :this_year, lambda {
    #TODO, reset this total at beginning of year
    where("created_at > ?", Date.today.beginning_of_year)
  }
  private
  def calculate_commission
    if self.transaction_made?
      amount = campaign.commission_rule.calculate_commission(self)
      due_date = Date.today + campaign.commission_rule.terms_value.send(campaign.commission_rule.terms_type)
      Balance.add(rep, self, amount, due_date)
    end
  end
end
