class AssetGroup < ActiveRecord::Base
  belongs_to :company
  has_and_belongs_to_many :assets
  has_and_belongs_to_many :campaigns
end
