class Group < ActiveRecord::Base
  has_and_belongs_to_many :reps
  has_and_belongs_to_many :campaigns
  belongs_to :company

  after_save :set_as_default


  def self.create_default_group(company)
    company.groups.create(:name => "Default Group", :default=> true)
  end

  private

  def set_as_default 
    if default == true
      company.groups.where("id != ?", id).each do |group|
        group.update_columns(default: false)
      end
    end
  end
end
