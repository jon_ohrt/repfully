class Portal < ActiveRecord::Base
  belongs_to :company
  mount_uploader :logo, LogoUploader
  has_one :training_overview, class_name: "Lesson", foreign_key: :training_overview_id 
  accepts_nested_attributes_for :training_overview
  after_initialize :create_training_overview

  private
  def create_training_overview
    self.build_training_overview if training_overview.nil?
  end
end
