$(document).on 'ready page:load', ->
  country_id = "rep_country"
  state_id = "rep_state" 
  $(".#{state_id}").on "chosen:ready", "select", (e)->
    $(e.target).val($('.selected-state').data("selected-state")).trigger("chosen:updated")
  
  CountryStateSelect({ use_chosen: true, country_id: country_id, state_id: state_id })
  if $("##{country_id} option:selected").length > 0
    $("##{country_id}").trigger("change")
