selectLessonPartType = (target)->
  parent = $(target).parents(".lesson-part")
  parent.find('[class^=part-type]').find('input, textarea, select').prop("disabled", true)
  parent.find("[class^=part-type]").hide()
  val = $(target).val()
  if val == 'text'
    parent.find(".part-type-text").find("input, textarea").prop("disabled", false)
    parent.find(".part-type-text").show()
  if val == 'video'
    parent.find(".part-type-video").find("input, textarea").prop("disabled", false)
    parent.find(".part-type-video").show()
  if val == 'resource'
    parent.find(".part-type-resource").find("input, textarea, select").prop("disabled", false)
    $(".link-type-selector").trigger("change")
    parent.find(".part-type-resource").show()

selectLinkType = (target)->
  if(target.val() == "url")
    target.parent().next().show()
    target.parent().next().next().hide()
  else
    target.parent().next().hide()
    target.parent().next().next().show()

$ ->

  $(".lesson-parts").on "change", ".lesson-part-selector", (e) ->
    selectLessonPartType(e.target)
  $(".lesson-part-selector").each (i, selector)->
    selectLessonPartType(selector)
  $(".lesson-parts").on "change", ".link-type-selector", (e) ->
    selectLinkType($(e.target))
  $(".action-item").on "click", (e)->
    lessonContainer = $(e.target).parents(".lesson-container")
    lessonID = lessonContainer.attr("id").replace("lesson-", "")
    $.post("/complete-lesson/#{lessonID}").success ()=>
    lessonContainer.find(".icon-checkbox").addClass("checked")
  $(".link-type-selector").trigger("change")
