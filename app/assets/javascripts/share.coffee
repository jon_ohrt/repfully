$ ->
  $('#sendables-table').dataTable({
    "bJQueryUI": true,
    "sDom": 'lfrtip'
  });
  $('#printables-table').dataTable({
    "bJQueryUI": true,
    "sDom": 'lfrtip'
  });


  $('#sharables-table').dataTable({
    "scrollY": "auto",
    "scrollX": true,
    "fnPreDrawCallback":  (oSettings) ->
      #// create a thumbs container if it doesn't exist. put it in the dataTables_scrollbody div
      if ($('#thumbs_container').length < 1)
        $('.dataTables_scrollBody').append("<div id='thumbs_container'></div>")

      #// clear out the thumbs container
      $('#thumbs_container').html('')

      return true
    ,
    "fnRowCallback": ( nRow, aData, iDisplayIndex, iDisplayIndexFull ) ->
      $('#thumbs_container').append(aData[0])
      return nRow
  })



