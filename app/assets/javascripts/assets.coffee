selectFields = (target)->
  $(".sendable-fields, .sharable-fields").hide()
  $(".sendable-fields input, .sharable-fields input").prop("disabled", true)
  if target.val() == 'sendable'
    $(".sendable-fields input").prop("disabled", false)
    $(".sendable-fields").show()
  else
    $(".sharable-fields input").prop("disabled", false)
    $(".sharable-fields").show()

$ ->
  $(".asset-form").on 'change', 'select', (e)->
    selectFields($(e.target))
  selectFields($(".asset-form select"))