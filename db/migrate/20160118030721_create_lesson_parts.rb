class CreateLessonParts < ActiveRecord::Migration
  def change
    create_table :lesson_parts do |t|
      t.integer :lesson_id
      t.integer :lesson_part_type
      t.text :text_section
      t.string :action_link_text
      t.string :action_link_url
      t.string :image

      t.timestamps null: false
    end
  end
end
