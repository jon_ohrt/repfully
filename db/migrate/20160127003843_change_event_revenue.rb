class ChangeEventRevenue < ActiveRecord::Migration
  def change
    remove_column :events, :revenue
    add_monetize :events, :revenue
  end
end
