class AddFieldsToReps < ActiveRecord::Migration
  def change
    add_column :reps, :sales_ytd, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :reps, :commission_ytd, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :reps, :sales_since_active, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :reps, :commission_since_active, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :reps, :unpaid_commissions, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :reps, :next_commission_due, :date
    add_column :reps, :slug, :string, uniq: true
    add_column :reps, :token, :string
  end
end
