class AddDefaultToProductGroups < ActiveRecord::Migration
  def change
    add_column :product_groups, :default, :boolean, default: false
  end
end
