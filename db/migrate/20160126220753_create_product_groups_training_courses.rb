class CreateProductGroupsTrainingCourses < ActiveRecord::Migration
  def change
    create_table :product_groups_training_courses do |t|
      t.integer :training_course_id
      t.integer :product_group_id
    end
  end
end
