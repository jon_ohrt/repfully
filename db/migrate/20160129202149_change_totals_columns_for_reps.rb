class ChangeTotalsColumnsForReps < ActiveRecord::Migration
  def change
    remove_column :reps, :sales_ytd
    remove_column :reps, :commission_ytd
    remove_column :reps, :sales_since_active
    remove_column :reps, :commission_since_active
    remove_column :reps, :unpaid_commissions

    add_monetize :reps, :sales_ytd
    add_monetize :reps, :commission_ytd
    add_monetize :reps, :sales_since_active
    add_monetize :reps, :commission_since_active
    add_monetize :reps, :unpaid_commissions

  end
end
