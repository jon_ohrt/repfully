class CreateCampaignsGroups < ActiveRecord::Migration
  def change
    create_table :campaigns_groups do |t|
      t.integer :campaign_id
      t.integer :group_id
    end
  end
end
