class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.integer :event_id
      t.integer :rep_id
      t.monetize :amount
      t.integer :amount_type

      t.timestamps null: false
    end
  end
end
