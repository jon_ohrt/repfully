class AddTrackingTokenToReps < ActiveRecord::Migration
  def change
    add_column :reps, :tracking_token, :string
  end
end
