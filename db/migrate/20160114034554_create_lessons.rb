class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.integer :training_course_id
      t.boolean :completed
      t.string :title

      t.timestamps null: false
    end
  end
end
