class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :rep_id
      t.integer :campaign_id
      t.integer :company_id
      t.string  :transaction_token
      t.integer :ip_address

      t.string  :name
      t.decimal :revenue, :precision => 8, :scale => 2
      t.integer :event_type
      t.text :products
      t.string :order_number

      t.timestamps null: false
    end
  end
end
