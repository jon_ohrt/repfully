class CreateGroupsReps < ActiveRecord::Migration
  def change
    create_table :groups_reps do |t|
      t.integer :group_id
      t.integer :rep_id
    end
  end
end
