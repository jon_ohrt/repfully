class CreateCommissionRules < ActiveRecord::Migration
  def change
    create_table :commission_rules do |t|
      t.decimal :commission_value
      t.integer :commission_type
      t.integer :terms_value
      t.integer :terms_type
      t.integer :campaign_id

      t.timestamps null: false
    end
  end
end
