class AddShowIconAndLinkTypeToLessonPart < ActiveRecord::Migration
  def change
    add_column :lesson_parts, :show_icon, :boolean
    add_column :lesson_parts, :link_type, :integer
  end
end
