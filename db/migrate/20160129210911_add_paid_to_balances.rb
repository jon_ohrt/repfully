class AddPaidToBalances < ActiveRecord::Migration
  def change
    add_column :balances, :paid, :boolean, default: :false
  end
end
