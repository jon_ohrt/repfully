class AddActionLinkFileToLessonPart < ActiveRecord::Migration
  def change
    add_column :lesson_parts, :action_link_file, :string
  end
end
