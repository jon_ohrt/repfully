class AddDueDateToBalances < ActiveRecord::Migration
  def change
    add_column :balances, :due_date, :date
  end
end
