class AddAddressAndEmailToEvent < ActiveRecord::Migration
  def change
    add_column :events, :address, :string
    add_column :events, :email, :string
    add_column :events, :quantity, :integer
  end
end
