class CreateAssetsProductGroups < ActiveRecord::Migration
  def change
    create_table :assets_product_groups do |t|
      t.integer :asset_id
      t.integer :product_group_id
    end
  end
end
