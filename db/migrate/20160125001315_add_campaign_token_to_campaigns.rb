class AddCampaignTokenToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :campaign_token, :string
  end
end
