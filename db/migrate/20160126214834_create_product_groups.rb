class CreateProductGroups < ActiveRecord::Migration
  def change
    create_table :product_groups do |t|
      t.string :name
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
