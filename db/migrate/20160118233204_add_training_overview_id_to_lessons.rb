class AddTrainingOverviewIdToLessons < ActiveRecord::Migration
  def change
    add_column :lessons, :training_overview_id, :integer
  end
end
