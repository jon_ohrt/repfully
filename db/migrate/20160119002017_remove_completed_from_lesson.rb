class RemoveCompletedFromLesson < ActiveRecord::Migration
  def change
    remove_column :lessons, :completed
  end
end
