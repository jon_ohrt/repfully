class CreateAssetGroups < ActiveRecord::Migration
  def change
    create_table :asset_groups do |t|
      t.string :name
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
