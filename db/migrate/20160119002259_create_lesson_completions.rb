class CreateLessonCompletions < ActiveRecord::Migration
  def change
    create_table :lesson_completions do |t|
      t.integer :lesson_id
      t.integer :rep_id

      t.timestamps null: false
    end
  end
end
