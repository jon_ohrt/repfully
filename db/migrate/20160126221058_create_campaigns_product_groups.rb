class CreateCampaignsProductGroups < ActiveRecord::Migration
  def change
    create_table :campaigns_product_groups do |t|
      t.integer :campaign_id
      t.integer :product_group_id
    end
  end
end
