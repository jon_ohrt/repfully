class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.integer :asset_type
      t.string :title
      t.text :content
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
