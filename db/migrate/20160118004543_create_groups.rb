class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.text :description
      t.boolean :default
      t.integer :company_id
      t.timestamps null: false
    end
  end
end
