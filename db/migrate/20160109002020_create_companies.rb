class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :subdomain
      t.integer :commission_value
      t.string :commission_type
      t.integer :terms_value
      t.string :terms_type
      t.string :slug, uniq: true
      t.timestamps null: false
    end
  end
end
