class CreatePortals < ActiveRecord::Migration
  def change
    create_table :portals do |t|
      t.text :registration_content
      t.string :logo

      t.timestamps null: false
    end
  end
end
