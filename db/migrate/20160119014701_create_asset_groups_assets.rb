class CreateAssetGroupsAssets < ActiveRecord::Migration
  def change
    create_table :asset_groups_assets do |t|
      t.integer :asset_group_id
      t.integer :asset_id
    end
  end
end
