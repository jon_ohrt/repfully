class AddCompanyIdToPortal < ActiveRecord::Migration
  def change
    add_column :portals, :company_id, :integer
  end
end
