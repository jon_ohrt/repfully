class CreateAssetGroupsCampaigns < ActiveRecord::Migration
  def change
    create_table :asset_groups_campaigns do |t|
      t.integer :asset_group_id
      t.integer :campaign_id
    end
  end
end
