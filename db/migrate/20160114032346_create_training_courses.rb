class CreateTrainingCourses < ActiveRecord::Migration
  def change
    create_table :training_courses do |t|
      t.integer :course_number
      t.string  :title
      t.text    :description
      t.integer :company_id
      t.integer :campaign_id

      t.timestamps null: false
    end
  end
end
