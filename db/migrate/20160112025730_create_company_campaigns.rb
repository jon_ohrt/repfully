class CreateCompanyCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :name
      t.integer :company_id
      t.string :slug, uniq: true
      t.timestamps null: false
    end
  end
end
