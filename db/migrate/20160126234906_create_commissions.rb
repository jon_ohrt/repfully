class CreateCommissions < ActiveRecord::Migration
  def change
    create_table :commissions do |t|
      t.date :sale_date
      t.integer :customer_id
      t.monetize :sale_amount
      t.monetize :commission_amount
      t.date :commission_due_date
      t.integer :commission_status
      t.integer :rep_id
      t.timestamps null: false
    end
  end
end
