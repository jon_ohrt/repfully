Rails.application.routes.draw do
  
  

  post "tracking/order", to: "tracking#order"
  post "tracking/visit", to: "tracking#visit"

  # devise_for :reps
  devise_for :users, controllers: { registrations: "registrations" }
  resources :users

  resources :companies, only: [:new, :create]


  namespace :admin do
    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      resources dashboard_resource
    end
    root controller: DashboardManifest::ROOT_DASHBOARD, action: :index
  end

  namespace :company do
    resource :company
    resource :dashboard, controller: "dashboard"
    resource :portal
    resources :campaigns, except: [:show]
    resources :reps, except: [:show] do
      resources :balances
    end
    resources :groups, except: [:show]
    resources :assets
    resources :product_groups
    resources :training_courses, except: [:show] do
      resources :lessons
    end


  end

  authenticated :user do
    root to: "company/dashboard#show", as: :authenticated_root
  end

  constraints(Subdomain) do
    devise_for :reps, module: "portal"
    
    namespace :reps do
      get ":token/confirm", to: "/portal/confirmations#edit"
      patch ":token/confirm", to: "/portal/confirmations#update"
      resource :profile, :controller => "/portal/profile", :only => [:edit, :update]

    end

    resources :attachments, only: [:show], :controller => "portal/attachments"
    resource :learn, only: :show, as: :portal_learn, :controller => "portal/learn"
    resource :share, only: :show, as: :portal_share, :controller => "portal/share"
    resource :track, only: :show, as: :portal_track, :controller => "portal/track"
    get "inactive", to: "portal/unauthorized#inactive"
    post "complete-lesson/:lesson_id", to: "portal/learn#complete_lesson"
    root to: 'portal/track#show', as: :portal_root
  end
  
  
  root to: 'visitors#index'

end
